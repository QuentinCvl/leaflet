// Initialise la carte
var mymap = L.map('mapid');
// Ajuste les Coordonnées
mymap.setView([50.374159, 3.518704], 16);


// Ajoute le Layer sur la map et toutes les info requise
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic2hhZG93d2VyYSIsImEiOiJjazVjYXFkMTIxcDZ3M2xwZ2lyNnR0cjd0In0.sgikH2y5S77OvGZBcfD8QA', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 24,
    id: 'mapbox/streets-v11',

}).addTo(mymap);

// Ajoute un cercle autours de la localisation souhaité (Ici à POP School)
var circle = L.circle([50.374143, 3.518012], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.2,
    radius: 50
}).addTo(mymap);

// Change la map en fonction de l'adress
var button = document.getElementById('adress_button');
button.addEventListener("click", function () {

    var input = document.getElementById('adress_input').value;
    var changeInput = input.split(" ").join("%20");

    var newAdress = "https://us1.locationiq.com/v1/search.php?key=432ce7e4bc365b&q=" + changeInput + "&format=json";

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": newAdress,
        "method": "GET"
    }

    $.ajax(settings).done(function (response) {
        var newLat = response[0].lat;
        var newLon = response[0].lon;

        mymap.setView([newLat, newLon], 16);
        
        var adressLat = document.getElementById("adress_lat_input");
        var adressLon = document.getElementById("adress_lon_input");
        adressLat.value = newLat;
        adressLon.value = newLon;

        var marker = L.marker([newLat,newLon]).addTo(mymap);
        marker.bindPopup("Ici c'est <b>"+ input +"</b> Quoi !").openPopup();
    });


});

// Fonction qui va collecter les coordonnées de l'endroit où l'on clique
mymap.addEventListener("click", function (e) {

    var lat = document.getElementById('lat_input');
    var lon = document.getElementById('lon_input');

    // et les affiche dans les input selectionnés
    lat.value = e.latlng.lat;
    lon.value = e.latlng.lng;

    var newCoord = "https://us1.locationiq.com/v1/reverse.php?key=432ce7e4bc365b&lat=" + lat.value + "&lon="+ lon.value + "&format=json";

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": newCoord,
        "method": "GET"
    }

    $.ajax(settings).done(function (response) {
        var truc = response.display_name;

        var area = document.getElementById("adress_area");
        area.innerHTML = truc;
    });
});

// Bouton qui va changer la position de la carte avec les coordonnées récupérer juste avant
var changButton = document.getElementById("changeCoord");
changButton.addEventListener("click", function() {

    var lat = document.getElementById('lat_input').value;
    var lon = document.getElementById('lon_input').value;
    
    var newCoord = "https://us1.locationiq.com/v1/reverse.php?key=432ce7e4bc365b&lat=" + lat + "&lon="+ lon + "&format=json";

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": newCoord,
        "method": "GET"
    }

    $.ajax(settings).done(function (response) {
        var newLat = response.lat;
        var newLon = response.lon;

        mymap.setView([newLat, newLon], 16);
        
        var adressLat = document.getElementById("adress_lat_input");
        var adressLon = document.getElementById("adress_lon_input");
        adressLat.value = newLat;
        adressLon.value = newLon;

        
    });
    
});
// Permet d'ajouter un marqueur à l'endroit selectionné
var addMark = document.getElementById("placeMark");
addMark.addEventListener("click", function() {

    var lat = document.getElementById('lat_input').value;
    var lon = document.getElementById('lon_input').value;

    var marker = L.marker([lat, lon]).addTo(mymap);
    

});